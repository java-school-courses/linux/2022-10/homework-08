## Восьмое домашнее задание

## bash-скрипты

### Жёсткий deadline: 12 декабря 9:00

В директории `script` находится 2 файла:

- `chkport.sh`
- `bottlesong.sh`

Необходимо реализовать в них логику с помощью bash-скриптов на `shell` языке

### chkport.sh

#### Основное назначение

Проверить что порт открыт и прослушивается

#### Формат входных аргументов

```shell
./chkport.sh HOST PORT
```

Пример:

```shell
./chkport.sh localhost 80
```

#### Вывод приложения

##### Если порт открыт

```shell
Host HOST, port PORT available
```

Где `HOST` и `PORT` конкретные значения

Пример:

```shell
Host localhost, port 80 available
```

##### Если порт закрыт

```shell
Host HOST, port PORT not available
```

Пример:

```shell
Host localhost, port 80 not available
```

#### Обработка исключений

#### Неправильное число аргументов

```shell
Wrong number of input arguments: COUNT_ARGS. Two needed.
    Example:
./chkport.sh localhost 80
```

Где `COUNT_ARGS` - количество входных аргументов

Пример:

```shell
user@myhost:~$ ./chkport.sh 
Wrong number of input arguments: 0. Two needed.
    Example:
./chkport.sh localhost 80
```

```shell
user@myhost:~$ ./chkport.sh one two three
Wrong number of input arguments: 3. Two needed.
    Example:
./chkport.sh localhost 80
```

### bottlesong.sh

#### Основное назначение

Напечатать историю про упавшие бутылки

#### Формат входных аргументов

```shell
./bottlesong.sh NUMBER_OF_BOTTLES
```

Где `NUMBER_OF_BOTTLES` - количество строк о бутылках

Пример:

```shell
./bottlesong.sh 99
```

#### Вывод приложения

```shell
user@myhost:~$ ./bottlesong.sh 3
3 bottle(s) were on the table. One bottle fell on the floor. 2 bottle(s) left.
2 bottle(s) were on the table. One bottle fell on the floor. 1 bottle(s) left.
1 bottle(s) were on the table. One bottle fell on the floor. 0 bottle(s) left.
```

#### Обработка исключений

#### Неправильное число аргументов

```shell
Wrong number of input arguments: COUNT_ARGS. Only one needed.
Example:
    ./bottlesong.sh 99
```

Где `COUNT_ARGS` - количество входных аргументов

Пример:

```shell
user@myhost:~$ ./bottlesong.sh  
Wrong number of input arguments: 0. Only one needed.
Example:
    ./bottlesong.sh 99
```

```shell
user@myhost:~$ ./bottlesong.sh 1 2
Wrong number of input arguments: 2. Only one needed.
Example:
    ./bottlesong.sh 99
```

#### Неправильный тип аргумента

```shell
Wrong number: 'WRONG_NUMBER_OF_BOTTLES'. Need more then 0.
Example:
    ./bottlesong.sh 99
```

Где `WRONG_NUMBER_OF_BOTTLES` - неправильный входной аргумент

Пример:

```shell
user@myhost:~$ ./bottlesong.sh text
Wrong number: 'text'. Need more then 0.
Example:
    ./bottlesong.sh 99
```

```shell
user@myhost:~$ ./bottlesong.sh -1
Wrong number: '-1'. Need more then 0.
Example:
    ./bottlesong.sh 99
```

```shell
user@myhost:~$ ./bottlesong.sh 0
Wrong number: '0'. Need more then 0.
Example:
    ./bottlesong.sh 99
```

### Проверка

Сделайте Fork проекта с домашним заданием.\
Выполните задание, в каждом файле свой набор инструкций.\
После commit + push система GitLab отобразит статус проверки.\
Добейтесь, чтобы все автотесты прошли успешно.\
Сделайте Merge request в основной проект с домашним заданием, от которого вы сделали Fork.\
В Merge request напишите свои фамилию, имя, порядковый номер по ведомости, чтобы преподавателю было проще проставлять
вам зачёт за выполненное задание.\
Когда преподаватель посмотрит и примет задание, в Merge request появится **approve**. \
Принятое ДЗ - это `1` в ведомости на пересечении строки вашего имени и столбца текущего ДЗ.